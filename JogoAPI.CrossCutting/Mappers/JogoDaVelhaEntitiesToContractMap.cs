﻿using AutoMapper;
using JogoAPI.Domain.Contracts.JogoDaVelha;
using JogoAPI.Domain.Entities;

namespace JogoAPI.CrossCutting.Mappers
{
    public class JogoDaVelhaEntitiesToContractMap : Profile
    {
        public JogoDaVelhaEntitiesToContractMap()
        {
            //CreateMap<JogoDaVelhaEntities, JogoDaVelhaRequest>().ReverseMap();
            CreateMap<JogoDaVelhaEntities, JogoDaVelhaResponse>().ReverseMap();
        }
    }
}
