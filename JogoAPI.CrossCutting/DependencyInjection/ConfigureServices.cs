﻿using JogoAPI.Domain.Interfaces.Service;
using JogoAPI.Services;
using Microsoft.Extensions.DependencyInjection;

namespace JogoAPI.CrossCutting.DependencyInjection
{
    public static class ConfigureServices
    {
        public static void ConfigureDependenciesService(IServiceCollection serviceCollection)
        {
            serviceCollection.AddScoped<IJogoDaVelhaService, JogoDaVelhaService>();
        }
    }
}
