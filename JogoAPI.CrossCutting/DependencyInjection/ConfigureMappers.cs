﻿using JogoAPI.CrossCutting.Mappers;
using Microsoft.Extensions.DependencyInjection;

namespace JogoAPI.CrossCutting.DependencyInjection
{
    public static class ConfigureMappers
    {
        public static void ConfigureDependenciesMapper(IServiceCollection serviceCollection)
        {
            var config = new AutoMapper.MapperConfiguration(cnf =>
            {
                cnf.AddProfile(new JogoDaVelhaEntitiesToContractMap());
            });

            var mapConfiguration = config.CreateMapper();
            serviceCollection.AddSingleton(mapConfiguration);
        }
    }
}
