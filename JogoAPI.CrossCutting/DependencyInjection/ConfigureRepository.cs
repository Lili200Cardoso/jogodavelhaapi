﻿using JogoAPI.Domain.Interfaces.Repository;
using JogoAPI.Repository;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace JogoAPI.CrossCutting.DependencyInjection
{
    public static class ConfigureRepository
    {
        public static void ConfigureDependenciesRepository(IServiceCollection serviceCollection, string connectionString)
        {
            serviceCollection.AddScoped<IJogoDaVelhaRepository, JogoDaVelhaRepository>();
            serviceCollection.AddDbContext<JogoAPIContext>(options => options.UseSqlServer(connectionString));
        }
    }
}
