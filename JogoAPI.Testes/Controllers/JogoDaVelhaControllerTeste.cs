﻿using Bogus;
using JogoAPI.Controllers;
using JogoAPI.Domain.Interfaces.Service;
using JogoAPI.Testes.Fakers;
using Moq;

namespace JogoAPI.Testes.Controllers
{

    [Trait("Controller", "Controller de JogosDaVelha")]
    public class JogoDaVelhaControllerTeste
    {
        private IJogoDaVelhaService _jogoDaVelhaService;
        private readonly Mock<IJogoDaVelhaService> _mockJogoDaVelhaService = new Mock<IJogoDaVelhaService>();

        private readonly JogoDaVelhaContractFaker _jogoDaVelhaRequestFake = new JogoDaVelhaContractFaker();


        [Fact(DisplayName = "Cria um novo jogo")]
        public async Task Post()
        {
            var jogoRequest = JogoDaVelhaContractFaker.JogoDaVelhaRequest();

            var resultJogoResponse = JogoDaVelhaContractFaker.JogoDaVelhaResponseBaseRequestAsync(jogoRequest.IniciarJogo);
            _mockJogoDaVelhaService.Setup(mock => mock.Post(jogoRequest)).Returns(resultJogoResponse);

            var controller = new JogoDaVelhaController(_mockJogoDaVelhaService.Object);
            var result = await controller.Post(jogoRequest);
            Assert.NotNull(result);
        }


        //[Fact(DisplayName = "Inicia um jogo vazio")]
        //public async Task Post()
        //{
        //    var jogoRequest = jogoDaVelhaContractFaker.JogoDaVelhaRequest();

        //    var resultJogoRequest = JogoDaVelhaContractFaker.JogoDaVelhaResponseBaseRequestAsync(jogoRequest.Jogador);

        //    var controller = new JogoDaVelhaController(_mockJogoDaVelhaService.Object);

        //    var result = await controller.Post(jogoRequest);

        //    var ex = Assert.Throws<Exception>(() => JogoDaVelhaRequest(Post(jogoRequest.Jogador)));

        //    Assert.Equal("O Objeto responsável está vazio!", ex.Message);
        //}


        //[Fact(DisplayName = "Lista todos os Jogos")]
        //public async Task Get()
        //{
        //    _mockJogoDaVelhaService.Setup(mock => mock.Get()).Returns(JogoDaVelhaContractFaker.JogoDaVelhaResponseAsync());

        //    var controller = new JogoDaVelhaController(_mockJogoDaVelhaService.Object);

        //    var result = await controller.Get();

        //    Assert.True(result.ToList().Count() > 0);

        //}
        ////POSSO USAR UM GETBYPOSICION
        //[Fact(DisplayName = "Busca um jogo por Id")]
        //public async Task GetById()
        //{
        //    int id = JogoDaVelhaContractFaker.GetId();

        //    _mockJogoDaVelhaService.Setup(mock => mock.GetById(id)).Returns(JogoDaVelhaContractFaker.JogoDaVelhaResponseAsync(id));

        //    var controller = new JogoDaVelhaController(_mockJogoDaVelhaService.Object);

        //    var result = await controller.GetById(id);

        //    Assert.Equal(result.Id, id);

        //}




        //[Fact(DisplayName = "Edita um jogo já existente")]
        //public async Task Put()
        //{
        //    var jogoRequest = JogoDaVelhaContractFaker.JogoDaVelhaRequest();

        //    var resultJogoRequest = JogoDaVelhaContractFaker.JogoDaVelhaResponseBaseRequestAsync(jogoRequest.Nome);

        //    _mockJogoDaVelhaService.Setup(mock => mock.Put(jogoRequest, resultJogoRequest.Result.Id)).Returns(resultJogoRequest);

        //    var controller = new JogoDaVelhaController(_mockJogoDaVelhaService.Object);

        //    var result = await controller.Put(resultJogoRequest.Result.Id, jogoRequest);

        //    Assert.Equal(result.Nome, resultJogoRequest.Result.Nome);

        //}

        //[Fact(DisplayName = "Remove um jogo existente")]
        //public async Task Delete()
        //{
        //    int id = JogoDaVelhaContractFaker.GetId();

        //    _mockJogoDaVelhaService.Setup(mock => mock.Delete(id)).Returns(() => Task.FromResult(string.Empty));

        //    var controller = new JogoDaVelhaController(_mockJogoDaVelhaService.Object);

        //    try
        //    {
        //        await controller.Delete(id);
        //    }
        //    catch (System.Exception)
        //    {
        //        Assert.True(false);
        //    }


        //}


    }
}
