﻿using AutoMapper;
using JogoAPI.Domain.Contracts.JogoDaVelha;
using JogoAPI.Domain.Entities;
using JogoAPI.Domain.Interfaces.Repository;
using JogoAPI.Services;
using JogoAPI.Testes.Fakers;
using Moq;
using static JogoAPI.Testes.CrossCutting.BaseAutoMapperFixture;

namespace JogoAPI.Testes.Services
{
    public class JogoDaVelhaServiceTeste
    {
        private readonly Mock<IJogoDaVelhaRepository> _mockJogoDaVelhaRepository = new Mock<IJogoDaVelhaRepository>();
        private readonly Mock<ITabuleiroRepository> _mockTabuleiroRepository = new Mock<ITabuleiroRepository>();
        private IMapper mapper = new AutoMapperFixture().GetMapper();

        //[Fact(DisplayName = "Lista todos os Jogos")]
        //public async Task Get()
        //{
        //    _mockJogoDaVelhaRepository.Setup(mock => mock.Get()).Returns(JogoDaVelhaEntitiesFaker.JogoDaVelhaEntitiesAsync());

        //    var service = new JogoDaVelhaService(_mockJogoDaVelhaRepository.Object, mapper);

        //    var result = await service.Get();

        //    Assert.True(result.ToList().Count() > 0);

        //}
        //////POSSO USAR UM GETBYPOSICION
        //[Fact(DisplayName = "Busca um jogo por Id")]
        //public async Task GetById()
        //{
        //    int id = JogoDaVelhaEntitiesFaker.GetId();

        //    _mockJogoDaVelhaRepository.Setup(mock => mock.GetById(id)).Returns(JogoDaVelhaEntitiesFaker.JogoDaVelhaEntitiesAsync(id));

        //    var service = new JogoDaVelhaService(_mockJogoDaVelhaRepository.Object, mapper);

        //    var result = await service.GetById(id);

        //    Assert.Equal(result.Id, id);

        //}

        [Fact(DisplayName = "Inicia um novo jogo")]
        public async Task Post()
        {
            var jogoRequest = JogoDaVelhaContractFaker.JogoDaVelhaRequest();
            var jogoRequestEntities = JogoDaVelhaEntitiesFaker.JogoDaVelhaEntities();
            var resultJogoRequest = JogoDaVelhaEntitiesFaker.JogoDaVelhaEntitiesBaseRequestAsync(jogoRequestEntities);
            _mockJogoDaVelhaRepository.Setup(mock => mock.Post(It.IsAny<JogoDaVelhaEntities>())).Returns(resultJogoRequest);

            var tabuleiroRequest = TabuleiroContractFaker.TabuleiroRequest();
            var tabuleiroRequestEntities = TabuleiroEntitiesFaker.TabuleiroEntities();
            var resultTabuleiroRequest = TabuleiroEntitiesFaker.TabuleiroEntitiesBaseRequestAsync(tabuleiroRequestEntities);
            _mockTabuleiroRepository.Setup(mock => mock.Post(It.IsAny<TabuleiroEntities>())).Returns(resultTabuleiroRequest);

            var service = new JogoDaVelhaService(_mockJogoDaVelhaRepository.Object, _mockTabuleiroRepository.Object, mapper);
            var result = await service.Post(jogoRequest);

            Assert.Equal(jogoRequestEntities.IdJogo, result.IdJogo);
        }


        //[Fact(DisplayName = "Edita um jogo já existente")]
        //public async Task Put()
        //{
        //    var jogoRequest = JogoDaVelhaContractFaker.JogoDaVelhaRequest();
        //    var jogoRequestEntities = JogoDaVelhaEntitiesFaker.JogoDaVelhaEntities();
        //    var resultJogoRequest = JogoDaVelhaEntitiesFaker.JogoDaVelhaEntitiesBaseRequestAsync(jogoRequestEntities.Nome);

        //    _mockJogoDaVelhaRepository.Setup(mock => mock.GetById(It.IsAny<int>())).Returns(resultJogoRequest);
        //    _mockJogoDaVelhaRepository.Setup(mock => mock.Put(It.IsAny<JogoDaVelhaEntities>(), It.IsAny<int?>())).Returns(resultJogoRequest);

        //    var controller = new JogoDaVelhaService(_mockJogoDaVelhaRepository.Object, mapper);

        //    var result = await controller.Put(jogoRequest, resultJogoRequest.Result.Id);

        //    Assert.Equal(result.Nome, resultJogoRequest.Result.Nome);

        //}

        //[Fact(DisplayName = "Remove um jogo existente")]
        //public async Task Delete()
        //{
        //    int id = JogoDaVelhaEntitiesFaker.GetId();

        //    _mockJogoDaVelhaRepository.Setup(mock => mock.Delete(id)).Returns(() => Task.FromResult(string.Empty));

        //    var controller = new JogoDaVelhaService(_mockJogoDaVelhaRepository.Object, mapper);

        //    try
        //    {
        //        await controller.Delete(id);
        //    }
        //    catch (System.Exception)
        //    {
        //        Assert.True(false);
        //    }


        //}
    }
}
