﻿using Bogus;
using JogoAPI.Domain.Entities;

namespace JogoAPI.Testes.Fakers
{
    public class JogoDaVelhaEntitiesFaker
    {
        private static readonly Faker Fake = new Faker();

        //DEVOLVE UMA LISTA COM UM JOGO
        //private IEnumerable<JogoDaVelhaResponse> listaJogoDaVelhaResponses;

        //public static int GetId()
        //{
        //    return Fake.IndexFaker;
        //}

        ////DEVOLVENDO UMA LISTA COM VÁRIOS JOGOS(10)
        //public static async Task<IEnumerable<JogoDaVelhaEntities>> JogoDaVelhaEntitiesAsync()
        //{
        //    var minhaLista = new List<JogoDaVelhaEntities>();
        //    for (int i = 0; i < 10; i++)
        //    {
        //        minhaLista.Add(new JogoDaVelhaEntities()
        //        {
        //            Id = i,
        //            //Movimentos = Fake.PickRandom(1,9)
        //        });
        //    }
        //    return minhaLista;
        //}


        public static async Task<JogoDaVelhaEntities> JogoDaVelhaEntitiesAsync(int id)
        {
            return new JogoDaVelhaEntities()
            {
                Id = id
            };

        }

        //public static JogoDaVelhaRequest JogoDaVelhaRequest()
        //{
        //    return new JogoDaVelhaRequest()
        //    {
        //         var Movimentos = new Lista<JogoDaVelhaRequest>();
        //         for(int linha =0; linha <3; linha++)
        //       {   
        //          for(int coluna = 0; coluna < 3; coluna ++)
        //          {
        //             Movimentos[linha, coluna] = JogoDaVelhaRequest.Jogador.VAZIO;
        //          }
        //       }
        // 
        //    };
        //
        //}

        public static JogoDaVelhaEntities JogoDaVelhaEntities()
        {
            return new JogoDaVelhaEntities
            {
                IdJogo = Guid.NewGuid().ToString(),
                Status = Domain.Contracts.Enums.StatusJogoEnum.INICIADO
            };

        }

        public static async Task<JogoDaVelhaEntities> JogoDaVelhaEntitiesBaseRequestAsync(JogoDaVelhaEntities jogoDaVelhaEntity)
        {
            return new JogoDaVelhaEntities()
            {
                Id = Fake.IndexFaker,
                IdJogo = jogoDaVelhaEntity.IdJogo,
                Status = jogoDaVelhaEntity.Status
            };
        }

    }
}

