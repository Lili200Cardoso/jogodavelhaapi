﻿using Bogus;
using JogoAPI.Domain.Contracts.Enums;
using JogoAPI.Domain.Contracts.JogoDaVelha;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JogoAPI.Testes.Fakers
{
    public class JogoDaVelhaContractFaker
    {
        private static readonly Faker Fake = new Faker();

        //DEVOLVE UMA LISTA COM UM JOGO
        //private IEnumerable<JogoDaVelhaResponse> listaJogoDaVelhaResponses;

        //public static int GetId()
        //{
        //    return Fake.IndexFaker;
        //}


        //public static JogoDaVelhaRequest JogoDaVelhaRequest()
        //{
        //    return new JogoDaVelhaRequest()
        //    {
        //        var movimentos = new Lista<JogoDaVelhaRequest>();
        //    for (int linha = 0; linha < 3; linha++)
        //    {
        //        for (int coluna = 0; coluna < 3; coluna++)
        //        {
        //            movimentos[linha, coluna] = JogoDaVelhaRequest.Jogador.VAZIO;
        //        }
        //    }

        //}



        //DEVOLVENDO UMA LISTA COM VÁRIOS JOGOS(10)
        //public static async Task<IEnumerable<JogoDaVelhaResponse>> JogoDaVelhaResponseAsync()
        //{
        //    var minhaLista = new List<JogoDaVelhaResponse>();
        //    for (int i = 0; i < 10; i++)
        //    {
        //        minhaLista.Add(new JogoDaVelhaResponse()
        //        {
        //            Id = i,
        //            //Movimentos = Fake.PickRandom(1,9)
        //        });
        //    }
        //    return minhaLista;
        //}


        //public static async Task<JogoDaVelhaResponse> JogoDaVelhaResponseAsync(int id)
        //{
        //    return new JogoDaVelhaResponse()
        //    {
        //        Id = id
        //    };

        //}

        public static JogoDaVelhaRequest JogoDaVelhaRequest()
        {
            return new JogoDaVelhaRequest
            {
                IniciarJogo = true
            };

        }

        public static async Task<JogoDaVelhaResponse> JogoDaVelhaResponseBaseRequestAsync(StatusJogoEnum statusJogo)
        {
            return new JogoDaVelhaResponse()
            {
                Id = Fake.IndexFaker,
                Status = statusJogo
            };
        }

    }
}
