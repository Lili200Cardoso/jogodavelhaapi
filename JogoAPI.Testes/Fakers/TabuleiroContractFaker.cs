﻿using Bogus;
using JogoAPI.Domain.Contracts.Tabuleiro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JogoAPI.Testes.Fakers
{
    public class TabuleiroContractFaker
    {
        private static readonly Faker Fake = new Faker();

        public static TabuleiroRequest TabuleiroRequest()
        {
            return new TabuleiroRequest
            {                
                IdJogo = Guid.NewGuid().ToString()
            };

        }

        public static async Task<TabuleiroResponse> TabuleiroResponseBaseRequestAsync(string idJogo)
        {
            return new TabuleiroResponse()
            {
                Id = Fake.IndexFaker,
                IdJogo = idJogo
            };
        }

        //public static async Task<IEnumerable<TabuleiroResponse>> TabuleiroResponseAsync()
        //{
        //    var minhaLista = new List<TabuleiroResponse>();
        //    for (int i = 0; i <= 2; i++)
        //    {
        //        for(int j = 0; j <= 2; j++)
        //        {
        //            minhaLista.Add(new TabuleiroResponse()
        //            {
        //                Linha = i,
        //                Coluna = j,
        //                //Movimentos = Fake.PickRandom(1,9)
        //            });

        //        }
             
        //    }
        //    return minhaLista;
        //}
    }
}
