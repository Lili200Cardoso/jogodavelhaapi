﻿using Bogus;
using JogoAPI.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JogoAPI.Testes.Fakers
{
    public class TabuleiroEntitiesFaker
    {
        private static readonly Faker Fake = new Faker();

        //DEVOLVE UMA LISTA COM UM JOGO
        //private IEnumerable<JogoDaVelhaResponse> listaJogoDaVelhaResponses;

        //public static int GetId()
        //{
        //    return Fake.IndexFaker;
        //}

        ////DEVOLVENDO UMA LISTA COM VÁRIOS JOGOS(10)
        //public static async Task<IEnumerable<JogoDaVelhaEntities>> JogoDaVelhaEntitiesAsync()
        //{
        //    var minhaLista = new List<JogoDaVelhaEntities>();
        //    for (int i = 0; i < 10; i++)
        //    {
        //        minhaLista.Add(new JogoDaVelhaEntities()
        //        {
        //            Id = i,
        //            //Movimentos = Fake.PickRandom(1,9)
        //        });
        //    }
        //    return minhaLista;
        //}


        public static async Task<TabuleiroEntities> TabuleiroEntitiesAsync(int id)
        {
            return new TabuleiroEntities()
            {
                Id = id
            };

        }

        //public static JogoDaVelhaRequest JogoDaVelhaRequest()
        //{
        //    return new JogoDaVelhaRequest()
        //    {
        //         var Movimentos = new Lista<JogoDaVelhaRequest>();
        //         for(int linha =0; linha <3; linha++)
        //       {   
        //          for(int coluna = 0; coluna < 3; coluna ++)
        //          {
        //             Movimentos[linha, coluna] = JogoDaVelhaRequest.Jogador.VAZIO;
        //          }
        //       }
        // 
        //    };
        //
        //}

        public static TabuleiroEntities TabuleiroEntities()
        {
            return new TabuleiroEntities
            {
                IdJogo = Guid.NewGuid().ToString(),
                //Status = Domain.Contracts.Enums.StatusJogoEnum.INICIADO
            };

        }

        public static async Task<TabuleiroEntities> TabuleiroEntitiesBaseRequestAsync(TabuleiroEntities tabuleiroEntity)
        {
            return new TabuleiroEntities()
            {
                Id = Fake.IndexFaker,
                IdJogo = tabuleiroEntity.IdJogo,
                //Status = tabuleiroEntity.Status
            };
        }
    }
}
