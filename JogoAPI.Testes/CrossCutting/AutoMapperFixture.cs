﻿using AutoMapper;
using JogoAPI.CrossCutting.Mappers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JogoAPI.Testes.CrossCutting
{
    public abstract class BaseAutoMapperFixture
    {
        public IMapper mapper { get; set; }

        public BaseAutoMapperFixture()
        {
            mapper = new AutoMapperFixture().GetMapper();
        }

        public class AutoMapperFixture : IDisposable
        {
            public IMapper GetMapper()
            {
                var config = new MapperConfiguration(cfg =>
                {
                    cfg.AddProfile(new JogoDaVelhaEntitiesToContractMap());
                });

                return config.CreateMapper();
            }

            public void Dispose() { }
        }
    }
  
}
