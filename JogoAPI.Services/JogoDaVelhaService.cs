﻿using AutoMapper;
using JogoAPI.Domain.Contracts.Enums;
using JogoAPI.Domain.Contracts.JogoDaVelha;
using JogoAPI.Domain.Entities;
using JogoAPI.Domain.Interfaces.Repository;
using JogoAPI.Domain.Interfaces.Service;

namespace JogoAPI.Services
{
    public class JogoDaVelhaService : IJogoDaVelhaService
    {
        public readonly IJogoDaVelhaRepository _jogoDaVelhaRepository;
        public readonly ITabuleiroRepository _tabuleiroRepository;
        public readonly IMapper _mapper;

        public JogoDaVelhaService(IJogoDaVelhaRepository jogoDaVelhaRepository, ITabuleiroRepository tabuleiroRepository,  IMapper mapper)
        {
            _jogoDaVelhaRepository = jogoDaVelhaRepository;
            _tabuleiroRepository = tabuleiroRepository;
            _mapper = mapper;
        }

        public async Task<JogoDaVelhaResponse> Post(JogoDaVelhaRequest jogoDaVelhaRequest)
        {
            //var requestJogoDaVelhaEntities = _mapper.Map<JogoDaVelhaEntities>(jogoDaVelhaRequest);

            try
            {
                if (jogoDaVelhaRequest.IniciarJogo)
                {
                    //Iniciando o Jogo
                    var jogoDaVelhaEntity = new JogoDaVelhaEntities()
                    {
                        IdJogo = Guid.NewGuid().ToString(),
                        DataInicioJogo = DateTime.Now,
                        Status = StatusJogoEnum.INICIADO
                    };

                    var jogoDaVelhaResponse = await _jogoDaVelhaRepository.Post(jogoDaVelhaEntity);

                    //Criando o tabuleiro
                    for (int i = 0; i <= 2; i++)
                    {
                        for (int j = 0; j <= 2; j++)
                        {
                            var tabuleiroEntity = new TabuleiroEntities()
                            {
                                IdJogo = jogoDaVelhaEntity.IdJogo,
                                Linha = i,
                                Coluna = j,
                                Jogador = JogadorEnum.VAZIO
                            };

                            var tabuleiroResponse = await _tabuleiroRepository.Post(tabuleiroEntity);
                        }
                    }

                    return _mapper.Map<JogoDaVelhaResponse>(jogoDaVelhaResponse);
                }
            }
            catch(Exception ex)
            {
               throw new Exception(ex.Message);
            }

            return _mapper.Map<JogoDaVelhaResponse>(null);
        }

        //public async Task<IEnumerable<JogoDaVelhaResponse>> Get()
        //{
        //    var listaJogosDaVelhaRetornoBaseDados =  await _jogoDaVelhaRepository.Get();

        //    return _mapper.Map<IEnumerable<JogoDaVelhaResponse>>(listaJogosDaVelhaRetornoBaseDados);
        //}

        //public async Task<JogoDaVelhaResponse> GetById(int id)
        //{
        //    var jogoDaVelhaRetornoBaseDados = await _jogoDaVelhaRepository.GetById(id);

        //    return _mapper.Map<JogoDaVelhaResponse>(jogoDaVelhaRetornoBaseDados);
        //}



        //TO DO : Mudar nome do método Post aqui


        //public async Task<JogoDaVelhaResponse> Put(JogoDaVelhaRequest jogoDaVelhaRequest, int? id)
        //{
        //    var jogoDaVelhaBancoDeDados = await _jogoDaVelhaRepository.GetById((int)id);

        //    jogoDaVelhaBancoDeDados.Nome = jogoDaVelhaRequest.Nome;

        //    var jogoDaVelhaCadastrado = await _jogoDaVelhaRepository.Put(jogoDaVelhaBancoDeDados, null);

        //    return _mapper.Map<JogoDaVelhaResponse>(jogoDaVelhaCadastrado);
        //}
        //public async Task Delete(int id)
        //{
        //    var jogoDaVelhaBancoDeDados = await _jogoDaVelhaRepository.GetById((int)id);

        //    if(jogoDaVelhaBancoDeDados != null)
        //    {
        //        await _jogoDaVelhaRepository.Delete(jogoDaVelhaBancoDeDados);
        //    }

        //}
    }
}