﻿using JogoAPI.Domain.Contracts.Enums;

namespace JogoAPI.Domain.Entities
{
    public class TabuleiroEntities
    {
        public int Id { get; set; }
        public string IdJogo { get; set; }
        public int Linha { get; set; }
        public int Coluna { get; set; }
        public JogadorEnum Jogador { get; set; }
    }
}
