﻿using JogoAPI.Domain.Contracts.Enums;

namespace JogoAPI.Domain.Entities
{
    public class JogoDaVelhaEntities
    {
        public int Id { get; set; }
        public string IdJogo { get; set; }
        public DateTime DataInicioJogo { get; set; }
        public StatusJogoEnum Status { get; set; }
    }
}
