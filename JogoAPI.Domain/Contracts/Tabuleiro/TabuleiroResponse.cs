﻿using JogoAPI.Domain.Contracts.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JogoAPI.Domain.Contracts.Tabuleiro
{
    public class TabuleiroResponse : TabuleiroRequest
    {
        public int Id { get; set; }
        public int Linha { get; set; }
        public int Coluna { get; set; }
        public JogadorEnum Jogador { get; set; }
    }
}
