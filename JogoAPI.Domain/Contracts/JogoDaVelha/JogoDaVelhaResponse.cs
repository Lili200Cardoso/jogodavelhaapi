﻿using JogoAPI.Domain.Contracts.Enums;

namespace JogoAPI.Domain.Contracts.JogoDaVelha
{
    public class JogoDaVelhaResponse : JogoDaVelhaRequest
    {
        public int Id { get; set; }
        public string IdJogo { get; set; }
        public DateTime DataInicioJogo { get; set; }
        public StatusJogoEnum Status { get; set; }
    }
}
