﻿namespace JogoAPI.Domain.Contracts.Enums
{
    public enum StatusJogoEnum
    {
        INICIADO,
        FINALIZADO
    }
}
