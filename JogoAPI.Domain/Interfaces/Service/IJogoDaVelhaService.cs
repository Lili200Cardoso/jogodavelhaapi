﻿using JogoAPI.Domain.Contracts.JogoDaVelha;

namespace JogoAPI.Domain.Interfaces.Service
{
    public interface IJogoDaVelhaService : IBaseCRUD<JogoDaVelhaRequest, JogoDaVelhaResponse>
    {
    }
}
