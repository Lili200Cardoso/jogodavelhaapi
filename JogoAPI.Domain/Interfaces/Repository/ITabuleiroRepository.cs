﻿using JogoAPI.Domain.Entities;

namespace JogoAPI.Domain.Interfaces.Repository
{
    public interface ITabuleiroRepository : IBaseCRUD<TabuleiroEntities, TabuleiroEntities>
    {
    }
}
