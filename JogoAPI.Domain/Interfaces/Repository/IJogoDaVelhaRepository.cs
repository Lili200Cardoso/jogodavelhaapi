﻿using JogoAPI.Domain.Entities;

namespace JogoAPI.Domain.Interfaces.Repository
{
    public interface IJogoDaVelhaRepository : IBaseCRUD<JogoDaVelhaEntities, JogoDaVelhaEntities>
    {
        //Task Delete(JogoDaVelhaEntities request);
    }
}
