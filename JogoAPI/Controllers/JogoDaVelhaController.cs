using JogoAPI.Domain.Contracts.JogoDaVelha;
using JogoAPI.Domain.Interfaces.Service;
using Microsoft.AspNetCore.Mvc;

namespace JogoAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class JogoDaVelhaController : ControllerBase
    {
        public readonly IJogoDaVelhaService _jogoDaVelhaService;

        public JogoDaVelhaController(IJogoDaVelhaService jogoDaVelhaService)
        {
            _jogoDaVelhaService = jogoDaVelhaService;
        }

        [HttpPost]
        public async Task<JogoDaVelhaResponse> Post(JogoDaVelhaRequest jogoDaVelhaRequest)
        {
            return await _jogoDaVelhaService.Post(jogoDaVelhaRequest);
        }

        //[HttpGet]
        //public async Task<IEnumerable<JogoDaVelhaResponse>> Get()
        //{
        //    return await _jogoDaVelhaService.Get();
        //}

        //[HttpGet("{id}")]
        //public async Task<JogoDaVelhaResponse> GetById(int id)
        //{
        //    return await _jogoDaVelhaService.GetById(id);
        //}



        //[HttpPut("{id}")]
        //public async Task<JogoDaVelhaResponse> Put(int id, JogoDaVelhaRequest jogoDaVelhaRequest)
        //{
        //    return await _jogoDaVelhaService.Put(jogoDaVelhaRequest, id);
        //}

        //[HttpDelete("{id}")]
        //public async Task Delete(int id)
        //{
        //    await _jogoDaVelhaService.Delete(id);
        //}
    }
}