﻿using JogoAPI.Domain.Entities;
using JogoAPI.Domain.Interfaces.Repository;
using Microsoft.EntityFrameworkCore;

namespace JogoAPI.Repository
{
    public class JogoDaVelhaRepository : IJogoDaVelhaRepository
    {
        private readonly JogoAPIContext _context;
        public JogoDaVelhaRepository(JogoAPIContext context)
        {
            _context = context; 
        }

        public async Task<JogoDaVelhaEntities> Post(JogoDaVelhaEntities jogoDaVelhaEntities)
        {
            await _context.JogosDaVelha.AddAsync(jogoDaVelhaEntities);
            await _context.SaveChangesAsync();
            return jogoDaVelhaEntities;
        }

        //public async Task<IEnumerable<JogoDaVelhaEntities>> Get()
        //{
        //    return await _context.JogosDaVelha.AsNoTracking().ToListAsync();
        //}

        //public async Task<JogoDaVelhaEntities> GetById(int id)
        //{
        //    return await _context.JogosDaVelha.Where(prop => prop.Id == id).AsNoTracking().FirstOrDefaultAsync();
        //}



        //public async Task<JogoDaVelhaEntities> Put(JogoDaVelhaEntities request, int? id = null)
        //{
        //    _context.JogosDaVelha.Update(request);
        //    await _context.SaveChangesAsync();
        //    return request;
        //}

        //public async Task Delete(JogoDaVelhaEntities request)
        //{
        //    _context.JogosDaVelha.Remove(request);
        //    await _context.SaveChangesAsync();
        //}

        //public Task Delete(int request)
        //{
        //    throw new NotImplementedException();
        //}
    }
}

   