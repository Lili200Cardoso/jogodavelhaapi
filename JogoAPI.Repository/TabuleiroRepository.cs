﻿using JogoAPI.Domain.Entities;
using JogoAPI.Domain.Interfaces.Repository;

namespace JogoAPI.Repository
{
    public class TabuleiroRepository : ITabuleiroRepository
    {
        private readonly JogoAPIContext _context;
        public TabuleiroRepository(JogoAPIContext context)
        {
            _context = context;
        }
        public async Task<TabuleiroEntities> Post(TabuleiroEntities tabuleiroEntities)
        {
            await _context.Tabuleiros.AddAsync(tabuleiroEntities);
            await _context.SaveChangesAsync();
            return tabuleiroEntities;
        }

        //public async Task<IEnumerable<TabuleiroEntities>> Get()
        //{
        //    return await _context.Tabuleiros.AsNoTracking().ToListAsync();
        //}

        //public async Task<TabuleiroEntities> GetById(int id)
        //{
        //    return await _context.Tabuleiros.Where(prop => prop.Id == id).AsNoTracking().FirstOrDefaultAsync();
        //}



        //public async Task<TabuleiroEntities> Put(TabuleiroEntities request, int? id = null)
        //{
        //    _context.Tabuleiros.Update(request);
        //    await _context.SaveChangesAsync();
        //    return request;
        //}

        //public async Task Delete(TabuleiroEntities request)
        //{
        //    _context.Tabuleiros.Remove(request);
        //    await _context.SaveChangesAsync();
        //}

        //public Task Delete(int request)
        //{
        //    throw new NotImplementedException();
        //}
    }
}
