﻿using JogoAPI.Domain.Entities;
using JogoAPI.Repository.Mappings;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace JogoAPI.Repository
{
    public class JogoAPIContext : DbContext
    {
        public JogoAPIContext() { }
        
        public JogoAPIContext(DbContextOptions<JogoAPIContext> options) : base(options) { }
        
        public DbSet<JogoDaVelhaEntities> JogosDaVelha { get; set; }
        public DbSet<TabuleiroEntities> Tabuleiros { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<JogoDaVelhaEntities>(new JogoDaVelhaEntitiesMap().Configure);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                IConfigurationRoot configuration = new ConfigurationBuilder()
                   .SetBasePath(Directory.GetCurrentDirectory())
                   .AddJsonFile("appsettings.json")                   
                   .Build();
                var connectionString = configuration.GetConnectionString("jogoAPI");
                optionsBuilder.UseSqlServer(connectionString);
            }
        }
    }
}
