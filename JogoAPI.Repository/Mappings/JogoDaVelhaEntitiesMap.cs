﻿using JogoAPI.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace JogoAPI.Repository.Mappings
{
    public class JogoDaVelhaEntitiesMap : IEntityTypeConfiguration<JogoDaVelhaEntities>
    {
        public void Configure(EntityTypeBuilder<JogoDaVelhaEntities> builder)
        {
            builder.HasKey(x => x.Id);

            builder.ToTable("Jogos");
        }
    }
}
